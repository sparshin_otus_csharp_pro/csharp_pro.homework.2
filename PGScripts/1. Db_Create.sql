﻿-- Database: FakeAvitoDb

-- DROP DATABASE IF EXISTS "FakeAvitoDb";

CREATE DATABASE "FakeAvitoDb"
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;

COMMENT ON DATABASE "FakeAvitoDb"
    IS 'Database for homework1';