﻿INSERT INTO public."Goods"(
	"Name", "Description", "Weight", "Price")
	VALUES 
		('Наушники', 'Беспроводные, Bluetooth', 0.1, 1000),
		('Холодильник', 'Морозит как твоя бывшая', 80, 30000),
		('Телевизор', 'Не бит, не крашен', 20, 30000),
		('Робот-пылесос', 'Третье поколение', 5, 10000),
		('Письменный стол', 'Имеются потертости и сколы', 10, 8000)
		;


INSERT INTO public."Users"(
	"Email", "FirstName", "LastName")
	VALUES 
		('mail1@mail.com', 'Иван', 'Иванов'),
		('mail2@mail.com', 'Петр', 'Петров'),
		('mail3@mail.com', 'Сидор', 'Сидоров'),
		('mail4@mail.com', 'Василий', 'Васильев'),
		('mail5@mail.com', 'Федор', 'Федоров')
		;


INSERT INTO public."Orders"(
	"CreationDate", "ModificationDate", "UserId", "Comment")
	VALUES 
		( CURRENT_DATE, NULL, 1, 'Позвонить по телефону 555-55-55'),
		( CURRENT_DATE, NULL, 2, 'Без комментариев'),
		( CURRENT_DATE, NULL, 3, NULL),
		( CURRENT_DATE, NULL, 4, NULL),
		( CURRENT_DATE, NULL, 5, 'Не кантовать!')
	;


INSERT INTO public."OrdersGoods"(
	"OrderId", "GoodId", "Quantity")
	VALUES 
		(1, 1, 2),
		(1, 2, 1),
		(2, 3, 1),
		(3, 4, 1),
		(4, 5, 1),
		(5, 3, 2),
		(4, 2, 2),
		(5, 1, 2)
		;
