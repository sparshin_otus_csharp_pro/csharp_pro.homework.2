﻿-- Table: public.Users

-- DROP TABLE IF EXISTS public."Users";

CREATE TABLE IF NOT EXISTS public."Users"
(
    "Id" bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    "Email" text COLLATE pg_catalog."default" NOT NULL,
    "FirstName" text COLLATE pg_catalog."default" NOT NULL,
    "LastName" text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "Users_pkey" PRIMARY KEY ("Id")
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public."Users"
    OWNER to postgres;

-- Table: public.Goods

-- DROP TABLE IF EXISTS public."Goods";

CREATE TABLE IF NOT EXISTS public."Goods"
(
    "Id" bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    "Name" text COLLATE pg_catalog."default" NOT NULL,
    "Description" text COLLATE pg_catalog."default",
    "Weight" numeric NOT NULL DEFAULT 0,
    "Price" money NOT NULL DEFAULT 0,
    CONSTRAINT "Goods_pkey" PRIMARY KEY ("Id")
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public."Goods"
    OWNER to postgres;

-- Table: public.Orders

-- DROP TABLE IF EXISTS public."Orders";

CREATE TABLE IF NOT EXISTS public."Orders"
(
    "Id" bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    "CreationDate" date NOT NULL,
    "ModificationDate" date,
    "UserId" bigint NOT NULL,
    "Comment" text COLLATE pg_catalog."default",
    CONSTRAINT "Orders_pkey" PRIMARY KEY ("Id"),
    CONSTRAINT "Orders_UserId_fkey" FOREIGN KEY ("UserId")
        REFERENCES public."Users" ("Id") MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public."Orders"
    OWNER to postgres;
-- Index: fki_FK_Or

-- DROP INDEX IF EXISTS public."fki_FK_Or";

CREATE INDEX IF NOT EXISTS "fki_FK_Or"
    ON public."Orders" USING btree
    ("UserId" ASC NULLS LAST)
    TABLESPACE pg_default;


-- Table: public.OrdersGoods

-- DROP TABLE IF EXISTS public."OrdersGoods";

CREATE TABLE IF NOT EXISTS public."OrdersGoods"
(
    "Id" bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    "OrderId" bigint NOT NULL,
    "GoodId" bigint NOT NULL,
    "Quantity" integer NOT NULL DEFAULT 1,
    CONSTRAINT "OrdersGoods_pkey" PRIMARY KEY ("Id"),
    CONSTRAINT "Orders" FOREIGN KEY ("OrderId")
        REFERENCES public."Orders" ("Id") MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT "OrdersGoods_GoodId_fkey" FOREIGN KEY ("GoodId")
        REFERENCES public."Goods" ("Id") MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public."OrdersGoods"
    OWNER to postgres;