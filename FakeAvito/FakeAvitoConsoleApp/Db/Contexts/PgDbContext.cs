﻿using FakeAvitoConsoleApp.Models;
using Microsoft.EntityFrameworkCore;

namespace FakeAvitoConsoleApp.Db.Contexts
{
    public class PgDbContext: DbContext
    {
        private readonly string _connectionString;

        public PgDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_connectionString);
        }

        public DbSet<Good> Goods { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderGood> OrdersGoods { get; set; }
        public DbSet<User> Users { get; set; }

    }
}
