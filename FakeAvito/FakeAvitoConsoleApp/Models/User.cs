﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FakeAvitoConsoleApp.Models
{
    public class User
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }

        [Column("Email")]
        public string? Email { get; set; }

        [Column("FirstName")]
        public string? FirstName { get; set; }

        [Column("LastName")]
        public string? LastName { get; set; }

        public override string ToString()
        {
            const string delimeter = "; ";
            return GetType().GetProperties()
                .Select(info => (info.Name, Value: info.GetValue(this, null) ?? "(null)"))
                .Aggregate(
                    new StringBuilder(),
                    (sb, pair) => sb.Append($"{pair.Name}: {pair.Value}").Append(delimeter),
                    sb => sb.ToString()) ;
        }
    }
}
