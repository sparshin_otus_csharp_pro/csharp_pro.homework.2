﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FakeAvitoConsoleApp.Models
{
    [Table("OrdersGoods")]
    public class OrderGood
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }
        
        [Column("OrderId")]
        public long OrderId { get; set; }

        [Column("GoodId")]
        public long GoodId { get; set; }

        [Column("Quantity")]
        public int Quantity { get; set; }

        public override string ToString()
        {
            const string delimeter = "; ";
            return GetType().GetProperties()
                .Select(info => (info.Name, Value: info.GetValue(this, null) ?? "(null)"))
                .Aggregate(
                    new StringBuilder(),
                    (sb, pair) => sb.Append($"{pair.Name}: {pair.Value}").Append(delimeter),
                    sb => sb.ToString());
        }
    }
}
