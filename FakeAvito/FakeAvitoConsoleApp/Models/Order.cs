﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FakeAvitoConsoleApp.Models
{
    [Table("Orders")]
    public class Order
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }

        [Column("CreationDate")]
        public DateTime CreationDate { get; set; }

        [Column("ModificationDate")]
        public DateTime? ModificationDate { get; set; }

        [Column("UserId")]
        public long UserId { get; set; }

        [Column("Comment")]
        public string? Comment { get; set; }

        public override string ToString()
        {
            const string delimeter = "; ";
            return GetType().GetProperties()
                .Select(info => (info.Name, Value: info.GetValue(this, null) ?? "(null)"))
                .Aggregate(
                    new StringBuilder(),
                    (sb, pair) => sb.Append($"{pair.Name}: {pair.Value}").Append(delimeter),
                    sb => sb.ToString());
        }
    }
}
