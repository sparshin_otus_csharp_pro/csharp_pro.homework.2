﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FakeAvitoConsoleApp.Models
{
    [Table(name: "Goods")]
    public class Good
    {
        [Key]
        [Column("Id")]
        public long Id { get; set; }

        [Column("Name")]
        public string? Name { get; set; }

        [Column("Description")]
        public string? Description { get; set; }

        [Column("Weight")]
        public decimal Weight { get; set; }

        [Column("Price")]
        public decimal Price { get; set; }

        public override string ToString()
        {
            const string delimeter = "; ";
            return GetType().GetProperties()
                .Select(info => (info.Name, Value: info.GetValue(this, null) ?? "(null)"))
                .Aggregate(
                    new StringBuilder(),
                    (sb, pair) => sb.Append($"{pair.Name}: {pair.Value}").Append(delimeter),
                    sb => sb.ToString());
        }
    }
}
