﻿using Microsoft.Extensions.Configuration;
using FakeAvitoConsoleApp.Db.Contexts;
using FakeAvitoConsoleApp.Models;

namespace FakeAvitoConsoleApp
{
    internal class Program
    {
        static string _connectionString { get; set; }
        static async Task Main(string[] args)
        {
            var configuration = new ConfigurationBuilder().AddJsonFile($"appsettings.json", optional: true, reloadOnChange: true);

            var config = configuration.Build();
            _connectionString = config.GetConnectionString("POSTGRES_CONNECTION");

            var exit = false;
            do
            {
                PrintCommandList();

                var command = Console.ReadLine();
                switch (command.ToLower())
                {
                    case "printalldata":
                        await ExecutetPrintAllDataCommandAsync();
                        break;
                    case "addtotable":
                        await ExecuteAddAddToTableCommandAsync();
                        break;
                    case "exit":
                        exit = true;
                        break;
                }

            }
            while (!exit);

            Console.WriteLine("Good bye!");
            Thread.Sleep(3000);
        }

        private static async Task ExecutetPrintAllDataCommandAsync()
        {
            try
            {
                using (var context = new PgDbContext(_connectionString))
                {
                    Console.WriteLine("*************************************************************************************");
                    Console.WriteLine("Users:");
                    context.Users.ToList().ForEach(t => Console.WriteLine(t.ToString()));
                    Console.WriteLine("*************************************************************************************");
                    Console.WriteLine("Goods");
                    context.Goods.ToList().ForEach(t => Console.WriteLine(t.ToString()));
                    Console.WriteLine("*************************************************************************************");
                    Console.WriteLine("Orders");
                    context.Orders.ToList().ForEach(t => Console.WriteLine(t.ToString()));
                    Console.WriteLine("*************************************************************************************");
                    Console.WriteLine("OrdersGoods");
                    context.OrdersGoods.ToList().ForEach(t => Console.WriteLine(t.ToString()));
                    Console.WriteLine("*************************************************************************************");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        private static async Task ExecuteAddAddToTableCommandAsync()
        {
            var exit = false;
            do
            {
                PrintTablesList();

                var command = Console.ReadLine();
                switch (command.ToLower())
                {
                    case "users":
                        await AddUser();
                        break;
                    case "goods":
                        await AddGood();
                        break;
                    case "orders":
                        await AddOrder();
                        break;
                    case "ordersgoods":
                        await AddOrderGood();
                        break;
                    case "exit":
                        exit = true;
                        break;
                }

            }
            while (!exit);
        }

        private static async Task AddUser()
        {
            using (var context = new PgDbContext(_connectionString))
            {
                var user = new User()
                {
                    Email = ReadPropertyValueFromConsole("Email"),
                    FirstName = ReadPropertyValueFromConsole("FirstName"),
                    LastName = ReadPropertyValueFromConsole("LastName"),
                };

                await context.Users.AddAsync(user);
                await context.SaveChangesAsync();
            }
        }

        private static async Task AddGood()
        {
            using (var context = new PgDbContext(_connectionString))
            {
                var good = new Good()
                {
                    Name = ReadPropertyValueFromConsole("Name"),
                    Description = ReadPropertyValueFromConsole("Description", false)
                };

                if (decimal.TryParse(ReadPropertyValueFromConsole("Weight"), out decimal weight))
                {
                    good.Weight = weight;
                }

                if (decimal.TryParse(ReadPropertyValueFromConsole("Price"), out decimal price))
                {
                    good.Price = price;
                }

                await context.Goods.AddAsync(good);
                await context.SaveChangesAsync();
            }
        }

        private static async Task AddOrder()
        {
            using (var context = new PgDbContext(_connectionString))
            {
                var order = new Order()
                {
                    Comment = ReadPropertyValueFromConsole("Comment", false),
                    CreationDate = DateTime.TryParse(ReadPropertyValueFromConsole("CreationDate"), out DateTime creationDate) 
                    ? creationDate 
                    : DateTime.Now
                };

                var modificationDateString = ReadPropertyValueFromConsole("ModificationDate");
                if (!string.IsNullOrEmpty(modificationDateString) && DateTime.TryParse(modificationDateString, out DateTime modificationDate))
                {
                    order.ModificationDate = modificationDate;
                }
                else
                {
                    order.ModificationDate = null;
                }

                var userIdString = ReadPropertyValueFromConsole("UserId");
                long.TryParse(userIdString, out long userId);
                if (!context.Users.Any(t => t.Id == userId))
                {
                    Console.WriteLine($"User with ID: {userIdString} not found");
                    return;
                }

                order.UserId = userId;

                await context.Orders.AddAsync(order);
                await context.SaveChangesAsync();
            }
        }

        private static async Task AddOrderGood()
        {
            using (var context = new PgDbContext(_connectionString))
            {
                var orderGood = new OrderGood();

                var orderIdString = ReadPropertyValueFromConsole("OrderId");
                long.TryParse(orderIdString, out long orderId);
                if (!context.Orders.Any(t => t.Id == orderId))
                {
                    Console.WriteLine($"Order with ID: {orderIdString} not found");
                    return;
                }

                orderGood.OrderId = orderId;

                var goodIdString = ReadPropertyValueFromConsole("GoodId");
                long.TryParse(goodIdString, out long goodId);
                if (!context.Goods.Any(t => t.Id == goodId))
                {
                    Console.WriteLine($"Good with ID: {goodIdString} not found");
                    return;
                }

                orderGood.GoodId = goodId;

                var quantityString = ReadPropertyValueFromConsole("Quantity");
                if (int.TryParse(quantityString, out int quantity))
                {
                    orderGood.Quantity = quantity;
                }

                await context.OrdersGoods.AddAsync(orderGood);
                await context.SaveChangesAsync();
            }
        }

        private static string ReadPropertyValueFromConsole(string propertyName, bool isRequired = true)
        {
            string? result;
            do
            {
                Console.WriteLine($"Input {propertyName} value");
                result = Console.ReadLine();
            }
            while (isRequired && (string.IsNullOrEmpty(result) || string.IsNullOrWhiteSpace(result)));

            return result;
        }

        private static void PrintTablesList()
        {
            Console.WriteLine("Select table from list or input Exit:");
            Console.WriteLine("Users");
            Console.WriteLine("Goods");
            Console.WriteLine("Orders");
            Console.WriteLine("OrdersGoods");
        }

        private static void PrintCommandList()
        {
            Console.WriteLine("Сommand list:");
            Console.WriteLine("PrintAllData - print all tables data");
            Console.WriteLine("AddToTable - add data to table");
            Console.WriteLine("Exit - exit the program");
        }
    }
}